﻿using mordhau_messenger_lib;
using System;
using System.Reflection;
using WindowsInput.Native;

namespace mordhau_messenger
{
    static class Program
    {
        public static void Main(string chatKey = "VK_Y", string configFile = "./config.json")
        {
            Console.WriteLine($"Mordhau messenger Version {Assembly.GetEntryAssembly().GetName().Version}");

            var messageWriter = new MessageWriter(configFile, ConvertToVkKey(chatKey));
            messageWriter.Run();

            Console.WriteLine("\nKey mappings: ");

            foreach (var message in messageWriter.Messages)
            {
                Console.WriteLine($"Key: {message.KeyCode}, Message: {message.Message}");
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to close...");
            Console.ReadKey();
        }

        private static VirtualKeyCode ConvertToVkKey(string key)
        {
            if (Enum.TryParse(key, out VirtualKeyCode vkey))
            {
                return vkey;
            }

            return VirtualKeyCode.VK_Y;
        }
    }
}
