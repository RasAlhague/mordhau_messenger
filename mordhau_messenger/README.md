# mordhau_messenger Version 0.0.2

## About

This program is just a simple program that writes a message in the mordhau chat.
Messages are currently set via a json file.

## Future plans

Grafical user interface to make usage easier.

## Install

There is no installing required for this app, but you need to have .net 5 Runtime installed.

You can download it under https://dotnet.microsoft.com/download/dotnet/5.0.

There you need the `.Net Desktop Runtime 5.0.X` https://dotnet.microsoft.com/download/dotnet/thank-you/runtime-desktop-5.0.3-windows-x64-installer.

And the `.Net Runtime 5.0.X` https://dotnet.microsoft.com/download/dotnet/thank-you/runtime-5.0.3-windows-x64-installer.

Make sure you either have the config.json file in the same directory as the mordhau_messenger.exe file
or if you start it via commandline with the parameter `--config-file <filename>`.

## Configure

If you have change the default chat key from `Y` to another key you need to start the program via commandline with the parameter `--chat-key <Virtual key code>`.
For example if your key for chat is `x` you need to start it with the following params:

`mordhau_messenger.exe --chat-key "VK_X"`

## Problems/Issues/Questions

If you run into any problems write me on discord.
For questions too.

## Resources

This is for the case you delete your config.json file.
Copy the content below into a new config.json file inside of the is program.

{
  "Messages": {
    "NUMPAD1": "Long Live Stalweidism!",
    "NUMPAD2": "The position swapper is currently broken. It will be fixed soon.",
    "NUMPAD3": "Please dont be rude.",
    "NUMPAD4": "Small advice, the 'n-word' is forbidden. Please stop it or an admin will be notified.",
    "NUMPAD5": "I am bird!"
  }
}