﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput.Native;

namespace mordhau_messenger_lib
{
    public class ConfigLine
    {
        public VirtualKeyCode KeyCode { get; set; }
        public string Message { get; set; }
    }
}
