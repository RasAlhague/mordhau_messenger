﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WindowsInput.Native;

namespace mordhau_messenger_lib
{
    public class MessageConfig
    {
        public List<ConfigLine> Messages { get; set; }

        public MessageConfig()
        {
            Messages = new List<ConfigLine>();
        }

        public static MessageConfig FromJson(string file)
        {
            string json = File.ReadAllText(file);

            return JsonConvert.DeserializeObject<MessageConfig>(json);
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
