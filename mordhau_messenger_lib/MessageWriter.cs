﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput.Native;

namespace mordhau_messenger_lib
{
    public class MessageWriter
    {
        private MessageConfig _config;
        private readonly WindowsInput.InputSimulator _inputSimulator;
        private readonly VirtualKeyCode _chatKey;

        public IEnumerable<ConfigLine> Messages { get { return _config.Messages; } }
        public bool IsRunning { get; private set; }

        public MessageWriter(string configFile, VirtualKeyCode chatKey)
        {
            _config = MessageConfig.FromJson(configFile);
            _inputSimulator = new WindowsInput.InputSimulator();
            _chatKey = chatKey;
            IsRunning = false;
        }

        public void Run()
        {
            if (IsRunning)
            {
                return;
            }

            Task.Run(() =>
            {
                IsRunning = true;

                while (IsRunning)
                {
                    var messagesToWrite = _config.Messages.Where(x => _inputSimulator.InputDeviceState.IsHardwareKeyDown(x.KeyCode));

                    foreach (var messageToWrite in messagesToWrite)
                    {
                        _inputSimulator.Keyboard.KeyPress(_chatKey);
                        Thread.Sleep(50);
                        _inputSimulator.Keyboard.TextEntry(messageToWrite.Message);
                        _inputSimulator.Keyboard.KeyPress(VirtualKeyCode.RETURN);

                        Thread.Sleep(1000);
                    }

                    Thread.Sleep(1);
                }
            });
        }

        public void Stop()
        {
            IsRunning = false;
        }

        public void ReloadConfig(string configFile)
        {
            if(IsRunning)
            {
                throw new InvalidOperationException("Reload is not allowed while the writer is running!");
            }

            _config = MessageConfig.FromJson(configFile);
        }
    }
}
