﻿using mordhau_messenger_lib;
using mordhau_messenger_view.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace mordhau_messenger_view.ViewModels
{
    public class AboutViewModel : ViewModelBase
    {
        public string ViewVersion { get; }
        public string LibVersion { get; }

        public ICommand CopyVersCommand { get; private set; }

        public AboutViewModel()
        {
            var viewVersion = Assembly.GetEntryAssembly().GetName().Version;
            var libVersion = typeof(MessageWriter).Assembly.GetName().Version;

            ViewVersion = viewVersion.ToString();
            LibVersion = libVersion.ToString();

            CopyVersCommand = new RelayCommand(CopyVersExecute, CopyVersCanExecute);
        }

        private void CopyVersExecute(object obj)
        {
            Clipboard.SetText($"Clientvers. {ViewVersion}, Libvers. {LibVersion}");
        }

        private bool CopyVersCanExecute(object arg)
        {
            return true;
        }
    }
}
