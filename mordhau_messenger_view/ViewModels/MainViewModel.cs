﻿using mordhau_messenger_lib;
using mordhau_messenger_view.Commands;
using mordhau_messenger_view.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace mordhau_messenger_view.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public MessageWriter MessageWriter { get; }
        private readonly ObservableCollection<ConfigLine> _lines;
        private const string configFilePath = "./config.json";
        private bool _isRunning;
        private bool _darkmode;

        public ObservableCollection<ConfigLine> Lines { get => _lines; }
        public ICommand StartCommand { get; }
        public ICommand ReloadCommand { get; }
        public ICommand StopCommand { get; }
        public ICommand SaveChangesCommand { get; }
        public ICommand AddCommand { get; }
        public ICommand RemoveCommand { get; }
        public ICommand AboutCommand { get; }
        public bool IsRunning
        {
            get { return _isRunning; }
            private set { SetProperty(ref _isRunning, value); }
        }
        public bool Darkmode
        {
            get { return _darkmode; }
            set { SetProperty(ref _darkmode, value); }
        }

        public MainViewModel()
        {
            _lines = new ObservableCollection<ConfigLine>();

            if (!File.Exists(configFilePath))
            {
                MessageConfig messageConfig = new MessageConfig();
                messageConfig.Messages.Add(new ConfigLine() { KeyCode = WindowsInput.Native.VirtualKeyCode.NUMPAD0, Message = "This is a samle" });
                File.WriteAllText(configFilePath, messageConfig.ToJson());
            }

            MessageWriter = new MessageWriter(configFilePath, WindowsInput.Native.VirtualKeyCode.VK_Y);
            UpdateListView();

            StartCommand = new RelayCommand(StartExecute, StartCanExecute);
            StopCommand = new RelayCommand(StopExecute, StopCanExecute);
            ReloadCommand = new RelayCommand(ReloadExecute, ReloadCanExecute);
            SaveChangesCommand = new RelayCommand(SaveChangesExecute, SaveChangesCanExecute);
            AddCommand = new RelayCommand(AddExecute, AddCanExecute);
            RemoveCommand = new RelayCommand(RemoveExecute, RemoveCanExecute);
            AboutCommand = new RelayCommand(AboutExecute, AboutCanExecute);

            Darkmode = false;
            IsRunning = false;
        }

        private bool AboutCanExecute(object arg)
        {
            return true;
        }

        private void AboutExecute(object obj)
        {
            AboutView aboutView = new AboutView();
            aboutView.ShowDialog();
        }

        private bool RemoveCanExecute(object arg)
        {
            return true;
        }

        private void RemoveExecute(object obj)
        {
            var index = (int)obj;

            if (index >= 0)
            {
                _lines.RemoveAt(index);
            }
        }

        private void AddExecute(object obj)
        {
            _lines.Add(new ConfigLine() { KeyCode = WindowsInput.Native.VirtualKeyCode.NUMPAD0, Message = "Empty" });
        }

        private bool AddCanExecute(object arg)
        {
            return true;
        }

        private void UpdateListView()
        {
            _lines.Clear();
            foreach (var line in MessageWriter.Messages)
            {
                _lines.Add(line);
            }
        }

        private bool SaveChangesCanExecute(object arg)
        {
            return true;
        }

        private void SaveChangesExecute(object obj)
        {
            SaveChanges();
        }

        private bool ReloadCanExecute(object arg)
        {
            return true;
        }

        private void ReloadExecute(object obj)
        {
            MessageWriter.Stop();
            IsRunning = false;
            SaveChanges();

            MessageWriter.ReloadConfig(configFilePath);
            UpdateListView();

            MessageWriter.Run();
            IsRunning = true;
        }

        private void SaveChanges()
        {
            var config = new MessageConfig();
            foreach (var line in _lines)
            {
                config.Messages.Add(line);
            }
            File.WriteAllText(configFilePath, config.ToJson());
        }

        private bool StopCanExecute(object arg)
        {
            return MessageWriter.IsRunning;
        }

        private void StopExecute(object obj)
        {
            MessageWriter.Stop();
            IsRunning = false;
        }

        private bool StartCanExecute(object arg)
        {
            return !MessageWriter.IsRunning;
        }

        private void StartExecute(object obj)
        {
            MessageWriter.Run();
            IsRunning = true;
        }
    }
}
